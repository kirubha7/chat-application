<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class AddFriend extends Model
{
    public static function checkFriend($id)
    {

        return AddFriend::Where([['request_sender',Auth::user()->id],['request_receiver',$id]])->orWhere([['request_sender',$id],['request_receiver',Auth::user()->id]])->first();
    }
}

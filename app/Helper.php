<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AddFriend;
use Auth;
use App\User;
class Helper extends Model
{
    public  static function NotificationCount()
    {
      return AddFriend::where([['request_receiver',Auth::user()->id],['request_status','1']])->count();
    }

    public static function getSenderName($id)
    {
      return User::where('id',$id)->first()->name;
    }

    public static function getUserName($id){
      return User::where('id',$id)->first()->name;
    }

    public static function getNewMessages($id){
      $message = Message::where('add_friends_id',$id)->orderBy('id', 'desc')->first();
      if(!empty($message)){
        if(is_array(json_decode($message->read_staus)) && in_array(Auth::user()->id,json_decode($message->read_staus))){
            return 1;
        }else{
          return 2;
        }
      }else{
          return 3;
      }
    }

    public static function getChatCount()
    {
      $friendLists =   AddFriend::orWhere('request_receiver',Auth::User()->id)->orWhere('request_sender',Auth::User()->id)->where('request_status','2')->get();
      $count =0;
      foreach ($friendLists as $key => $friendList) {
        $message[$key] =  Message::where('add_friends_id',$friendList->id)->orderBy('id', 'desc')->first();
          if(!empty($message)){
            if(is_array(json_decode($message[$key]->read_staus)) && in_array(Auth::user()->id,json_decode($message[$key]->read_staus))){
                 $count;
            }else{
                 ++$count;
            }
          }else{
                 $count;
          }
        }
        return $count;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\AddFriend;
use App\Message;
use App\Helper;
use App\User;
class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = AddFriend::where('request_sender',Auth::user()->id)->orwhere('request_receiver',Auth::user()->id)->where('request_status','2')->orderBy('id','desc')->get();
        return view('chat.message.friends-list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = request('friend_id');
        $addFriend_id = AddFriend::orWhere([['request_sender',Auth::user()->id],['request_receiver',$id]])->orWhere([['request_sender',$id],['request_receiver',Auth::user()->id]])->where('request_status','2')->first()->id;
        $message = new Message();
        $message->add_friends_id = $addFriend_id;
        $message->user_id = Auth::user()->id;
        $message->message = request('message');
        $message->read_staus = json_encode(array(Auth::user()->id));
        $message->save();
        return request('message');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function chatWithFriend($id)
    {


      $friend_id =  User::whereid($id)->first()->id;
       $data['friendName'] = $id;
       $addFriend_id = AddFriend::orWhere([['request_sender',Auth::user()->id],['request_receiver',$id]])->orWhere([['request_sender',$id],['request_receiver',Auth::user()->id]])->where('request_status','2')->first()->id;
       $readStatusUpdate = Message::where('add_friends_id',$addFriend_id)->select(['id','read_staus'])->get();
       if(!$readStatusUpdate->isEmpty()){
         foreach ($readStatusUpdate as $key => $readStatusUpdates){
           if(is_array(json_decode($readStatusUpdates->read_staus)) && in_array(Auth::user()->id,json_decode($readStatusUpdates->read_staus))){

           }else{
             $readStatus = Message::find($readStatusUpdates->id);
             $readStatus->read_staus = json_encode(array($friend_id,Auth::user()->id));
             $readStatus->save();
           }
         }
       }
       $data['message'] = Message::where('add_friends_id',$addFriend_id)->get();
       return view('chat.message.chat',$data);
    }
}

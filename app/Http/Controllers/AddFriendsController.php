<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\AddFriend;

class AddFriendsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::where('id','!=',Auth::user()->id)->orderBy('id','desc')->get();
        return view('chat.add-friend.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
       /** Below stored Request function used for Stored the friend request */
    public function storeRequest($id)
    {
        $addFriend = new AddFriend();
        $addFriend->request_sender = Auth::user()->id;
        $addFriend->request_receiver = $id;
        $addFriend->request_status  = 1;
        $addFriend->save();
        return redirect(route('add-friends.index'))->with('success','Friend Request Send Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
        /** Show The Request List */
    public function requestList()
    {
      $data = AddFriend::where([['request_receiver',Auth::user()->id],['request_status',1]])->get();
      return view('chat.add-friend.request-list',compact('data'));
    }

    public function updateStatus(Request $request,$id)
    {
      $updateStatus = AddFriend::find($id);
      $updateStatus->request_status = request('status');
      $updateStatus->save();
      if(request('status') == 2){
          return redirect(route('requestList'))->with('success','Friend Request Accepted');
      }else if(request('status') == 3){
        return redirect(route('requestList'))->with('danger','Friend Request Rejected');
      }

    }
}

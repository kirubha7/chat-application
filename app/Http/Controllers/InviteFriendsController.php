<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\User;
use Auth;

class InviteFriendsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('chat.invite-friends.send-invite');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $email = request('email');
          $authName = Auth::user()->name;
          $chechUser = User::where('email',$email)->first();
          if(!empty($chechUser)){
            return redirect(route('invite-friends.index'))->with('danger','User Already Exists');
          }else{
            $data = [
                    'email'     =>$email,
                    'authName' =>$authName
                  ];
            Mail::send(['html'=>'chat.invite-friends.mail'], ["data"=>$data], function($message) {
                $message->from('kirubharaj555@gmail.com');
                $message->to(request('email'))->subject('Invite From Chat Application');

          });
          return redirect(route('invite-friends.index'))->with('success','Invite Send Successfully');
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

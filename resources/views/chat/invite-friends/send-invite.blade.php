@extends('chat.layouts.master')


@section('inner-content')


<div class="content">
<h1 class="content-heading">
Send Invite
</h1>


<div class="block block-rounded block-bordered">
        <div class="block-content">
                     <div class="row">
                       <div class="col-lg-3">
                           <p class="text-muted">

                           </p>
                       </div>
                       <div class="col-lg-8 col-xl-5">
                                    <!-- Form Labels on top - Default Style -->
                                    <form class="mb-5" action="{{ route('invite-friends.store') }}" method="POST" id="form">
                                        {{ csrf_field() }}

                                        <div class="form-group row">
                                            <label for="example-ltf-rate_per_kg" class="col-sm-4 col-form-label" >Enter Friend Email<span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                              <input type="email" class="form-control"  id="email" placeholder="Enter Your Friend Email" name="email">
                                           </div>
                                        </div>
                                        <div class="form-group row">
                                          <label for="example-ltf-total_rate" class="col-sm-4 col-form-label" ></label>
                                          <div class="col-sm-8">
                                            <button type="submit" data-toggle="click-ripple" class="btn btn-primary">Send Link</button>
                                            </div>
                                        </div>

                                </div>



                                    </form>
                                    <!-- END Form Labels on top - Default Style -->
                                </div>
                            </div>
                </div>
            </div>

@endsection


@section('javascript')

<script>

$(document).ready(function () {

  $('#form').validate({ // initialize the plugin

      rules: {

        email: {
            required: true,
            email : true
        },
      }
  });

});


</script>
@endsection

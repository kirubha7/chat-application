<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Chat Application</title>

          <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ asset('assets/media/favicons/apple-touch-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{asset('assets/media/favicons/ShareX_Icon_Full.ico')}}">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{asset('assets/js/plugins/datatables/dataTables.bootstrap4.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/plugins/select2/css/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">



        <!-- Fonts and Dashmix framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="{{asset('assets/css/dashmix.min.css')}}">
        <!-- END Stylesheets -->
    </head>

<body>

<div id="page-container" class="sidebar-o sidebar-dark enable-page-overlay side-scroll page-header-fixed page-header-dark">

@include('chat.layouts.sidebar')
@include('chat.layouts.header')

<!-- Main Container -->
<main id="main-container">

@yield('inner-content')

</main>
<!-- END Main Container -->


@include('chat.layouts.footer')

</div>

        <script src="{{asset('assets/js/dashmix.core.min.js')}}"></script>
        <script src="{{asset('assets/js/dashmix.app.min.js')}}"></script>
        <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

        <!-- Page JS Plugins -->
        <script src="{{asset('assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/datatables/buttons/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/datatables/buttons/buttons.print.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/datatables/buttons/buttons.html5.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/datatables/buttons/buttons.flash.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/datatables/buttons/buttons.colVis.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/select2/js/select2.full.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/jquery-validation/additional-methods.js')}}"></script>

        <!-- Page JS Code -->
        <script src="{{asset('assets/js/pages/be_forms_wizard.min.js')}}"></script>


        <!-- Page JS Code -->
        <script src="{{asset('assets/js/pages/be_tables_datatables.min.js')}}"></script>

        <!-- Page JS Plugins -->
        <script src="{{asset('assets/js/plugins/bootstrap-notify/bootstrap-notify.min.js')}}"></script>


        <!-- Page JS Plugins -->
        <script src="{{asset('assets/js/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins/chart.js/Chart.bundle.min.js')}}"></script>

        <!-- Page JS Code -->
        <script src="{{asset('assets/js/pages/be_pages_dashboard.min.js')}}"></script>

         @yield('javascript')


         <script>jQuery(function(){ Dashmix.helpers(['datepicker']); });</script>

        <script>



        @if(session()->has('success'))
            $(document).ready(function()
            {
                Dashmix.helpers('notify', {from: 'top', align: 'right',type: 'success', icon: 'fa fa-check mr-1', message: '{{ Session::get('success') }}'});
            });
        @endif

        @if(session()->has('danger'))
            $(document).ready(function()
            {
                Dashmix.helpers('notify', {from: 'top', align: 'right',type: 'danger', icon: 'fa fa-times mr-1', message: '{{ Session::get('danger') }}'});
            });
        @endif


        </script>

        <style>
          .error{
            border-color:#e04f1a;
            color:#e04f1a;
          }

          .alert-danger{
      color: #ffffff;
      background-color:#b52929;
    }

     .alert-success{
      color: #ffffff;
      background-color: #52b529;
    }

        </style>
</body>
</html>

<!-- Footer -->
<footer id="page-footer" class="bg-body-light">
    <div class="content py-0">
        <div class="row font-size-sm">
            <div class="col-sm-6 order-sm-1 text-center text-sm-left">
                Copyright &copy; {{ date('Y') }}</span>
            </div>
        </div>
    </div>
</footer>
<!-- END Footer -->

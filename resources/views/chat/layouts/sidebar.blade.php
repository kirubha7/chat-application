
       <nav id="sidebar" aria-label="Main Navigation">
           <!-- Side Header -->
           <div class="bg-header-dark">
               <div class="content-header bg-white-10">
                   <!-- Logo -->
                   <a class="link-fx font-w600 font-size-lg text-white" href="index.html">
                       <span class="smini-visible">
                           <span class="text-white-75">C</span><span class="text-white">A</span>
                       </span>
                       <span class="smini-hidden">
                           <span class="text-white-75">Chat</span><span class="text-white">Application</span>
                       </span>
                   </a>
                   <!-- END Logo -->

                   <!-- Options -->
                   <div>
                       <!-- Toggle Sidebar Style -->
                       <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                       <!-- Class Toggle, functionality initialized in Helpers.coreToggleClass() -->
                       <a class="js-class-toggle text-white-75" data-target="#sidebar-style-toggler" data-class="fa-toggle-off fa-toggle-on" data-toggle="layout" data-action="sidebar_style_toggle" href="javascript:void(0)">
                           <i class="fa fa-toggle-off" id="sidebar-style-toggler"></i>
                       </a>
                       <!-- END Toggle Sidebar Style -->

                       <!-- Close Sidebar, Visible only on mobile screens -->
                       <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                       <a class="d-lg-none text-white ml-2" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                           <i class="fa fa-times-circle"></i>
                       </a>
                       <!-- END Close Sidebar -->
                   </div>
                   <!-- END Options -->
               </div>
           </div>
           <!-- END Side Header -->

           <!-- Side Navigation -->
           <div class="content-side content-side-full">
               <ul class="nav-main">
                   <li class="nav-main-item">
                       <a class="nav-main-link {{ Route::is('dashboard') ? 'active' : '' }}" href="{{ route('dashboard')  }}" >
                           <i class="nav-main-link-icon si si-paper-plane"></i>
                           <span class="nav-main-link-name">Dashboard</span>
                       </a>
                   </li>
                   <li class="nav-main-item">
                       <a class="nav-main-link {{ Route::is('invite-friends.index') ? 'active' : '' }}" href="{{ route('invite-friends.index') }}" >
                       <i class="nav-main-link-icon si si-users"></i>
                           <span class="nav-main-link-name">Invite Friend</span>
                       </a>
                   </li>
                   <li class="nav-main-item">
                       <a class="nav-main-link {{ Route::is('add-friends.index') ? 'active' : '' }}" href="{{ route('add-friends.index') }}" >
                       <i class="nav-main-link-icon si si-user-follow"></i>
                           <span class="nav-main-link-name">Add Friend</span>
                       </a>
                   </li>
                   <li class="nav-main-item">
                     <a class="nav-main-link {{ Route::is('requestList') ? 'active' : '' }}"  href="{{ route('requestList') }}">
                                  <i class="nav-main-link-icon si si-bell"></i>
                                  <span class="nav-main-link-name">Notification<div class="nav-main-link-badge badge badge-pill badge-success">{{ App\Helper::NotificationCount() }}</div> </span>

                      </a>
                    </li>
                    <li class="nav-main-item">
                      <a class="nav-main-link {{ Route::is('chat.index') ? 'active' : '' }}"  href="{{ route('chat.index') }}">
                                   <i class="nav-main-link-icon si si-cup"></i>
                                   <span class="nav-main-link-name">Chat<div class="nav-main-link-badge badge badge-pill badge-success">{{ App\Helper::getChatCount() }}</div> </span>

                       </a>
                     </li>
               </ul>
           </div>
           <!-- END Side Navigation -->
       </nav>
       <!-- END Sidebar -->

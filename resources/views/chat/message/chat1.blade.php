@extends('chat.layouts.master')


@section('inner-content')


<div class="content">
<h1 class="content-heading">
Chat With Your Friend
</h1>
<div class="row">
  <div class="col-xl-3">
  </div>
                        <div class="col-xl-6">
                            <!-- Chat #1 -->
                            <div class="block block-rounded">
                                <!-- Chat #1 Header -->
                                <!-- <div class="block-content block-content-full bg-gd-fruit text-center">
                                    <img class="img-avatar img-avatar-thumb" src="{{ asset('assets/media/avatars/avatar10.jpg') }}" alt="">
                                    <p class="font-size-lg font-w600 text-white mt-3 mb-0">
                                        {{ ucfirst(App\Helper::getUserName($friendName)) }}
                                    </p>
                                </div> -->
                                <!-- END Chat #1 Header -->

                                <!-- Chat #1 Messages -->
                                <div class="js-chat-messages block-content block-content-full text-wrap-break-word overflow-y-auto" data-chat-id="1" style="height: 300px;">
                                  @foreach($message as $messages)
                                  @if(Auth::user()->id != $messages->user_id)

                                      <div class="mr-4">
                                        <div class="d-inline-block font-w600 animated fadeIn bg-body-light border-3x px-3 py-2 mb-2 shadow-sm mw-100 border-left border-dark rounded-right">
                                          {{ $messages->message }}
                                        </div>
                                     </div>
                                     <div class=" font-italic text-muted animated fadeIn my-2" style="font-size: x-small;">{{ $messages->created_at->diffForHumans()}}</div>
                                  @endif
                                  @if(Auth::user()->id == $messages->user_id)

                                      <div class="text-right ml-4">
                                            <div class="d-inline-block font-w600 animated fadeIn bg-body-light border-3x px-3 py-2 mb-2 shadow-sm mw-100 border-right border-primary rounded-left text-left">
                                              {{ $messages->message }}
                                        </div>
                                      </div>
                                      <div class=" font-italic text-muted animated fadeIn my-2 text-right" style="font-size: x-small;">{{ $messages->created_at->diffForHumans()}}</div>
                                @endif
                                   @endforeach
                                     <div id="appendMsg"></div>
                                </div>

                                <!-- Chat #1 Input -->
                                <div class="js-chat-form block-content p-2 bg-body-dark">
                                    <form action="{{ route('chat.store')  }}" method="POST" id="storemessage">
                                        <input type="text" name="message" class="js-chat-input form-control form-control-alt" data-target-chat-id="1" placeholder="Type a message..">
                                        <input type="hidden" value="{{ $friendName }}" name="friend_id">
                                    </form>
                                </div>
                                <!-- END Chat #1 Input -->
                            </div>
                            <!-- END Chat #1 -->
                        </div>
                    </div>
                    <div class="col-xl-3">
                    </div>
</div>
@endsection
@section('javascript')
<script>


$(document).ready(function () {
$(".js-chat-messages").animate({ scrollTop: $('.js-chat-messages').prop("scrollHeight")}, 1000);
$('#storemessage').on( 'submit', function(e) {
        e.preventDefault();
        var message = $(this).find('input[name=message]').val();
        var friend_id = $(this).find('input[name=friend_id]').val();
        $.ajax({
            type: "POST",
            url: '/chat/store',
            data: { 'message' : message ,'_token' : "{{ csrf_token() }}" , 'friend_id' : friend_id},
            success: function (message) {
                 //alert(message);

              var msg='<div class="text-right ml-4">'+
                  '<div class="d-inline-block font-w600 animated fadeIn bg-body-light border-3x px-3 py-2 mb-2 shadow-sm mw-100 border-right border-primary rounded-left text-left">'+
                        message
                  '</div>'+
                  '</div>';
                  $('#appendMsg').append(msg);
                  $('input[name=message]').val('');
                  $(".js-chat-messages").animate({ scrollTop: $('.js-chat-messages').prop("scrollHeight")}, 1000);
                  }
        });
        });

  $('#storemessage').validate({ // initialize the plugin
      rules: {
        message : {
          required : true,
        },
      }
  });

});
</script>
@endsection

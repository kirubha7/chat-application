@extends('chat.layouts.master')


@section('inner-content')


<div class="content">
<h1 class="content-heading">
Friends List
</h1>



                     <div class="row">
                      @foreach($data as $datas)
                       <div class="col-md-6 col-xl-3">
                          @if($datas->request_sender == Auth::user()->id)
                             <a class="block block-rounded block-link-pop" href="{{ route('chat.chatWithFriend',$datas->request_receiver) }}" target="_blank">
                                 <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                                     <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar9.jpg" alt="">
                                     <div class="ml-3 text-right">
                                         <p class="font-w600 mb-0">{{ ucfirst(App\Helper::getSenderName($datas->request_receiver))  }}</p>
                                         <p class="font-size-sm font-italic text-muted mb-0">
                                             @php($newMessage = App\Helper::getNewMessages($datas->id))
                                             @if($newMessage == 1)
                                                  <span class="badge badge-warning">No New Message</span>
                                             @elseif($newMessage == 2)
                                                  <span class="badge badge-success">New Message</span>
                                             @elseif($newMessage == 3)
                                                  <span class="badge badge-warning">No New Message</span>
                                             @endif

                                         </p>
                                     </div>
                                 </div>
                             </a>
                             @endif
                             @if($datas->request_receiver == Auth::user()->id)
                                <a class="block block-rounded block-link-pop" href="{{ route('chat.chatWithFriend',$datas->request_sender) }}" target="_blank">
                                    <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                                        <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar9.jpg" alt="">
                                        <div class="ml-3 text-right">
                                            <p class="font-w600 mb-0">{{ ucfirst(App\Helper::getSenderName($datas->request_sender))  }}</p>
                                            <p class="font-size-sm font-italic text-muted mb-0">
                                            @php($newMessage = App\Helper::getNewMessages($datas->id))
                                            @if($newMessage == 1)
                                                 <span class="badge badge-warning">No New Message</span>
                                            @elseif($newMessage == 2)
                                                 <span class="badge badge-success">New Message</span>
                                            @elseif($newMessage == 3)
                                                 <span class="badge badge-warning">No New Message</span>
                                            @endif
                                            </p>
                                        </div>
                                    </div>
                                </a>
                                @endif
                          </div>
                      @endforeach
                      </div>
@endsection

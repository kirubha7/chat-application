@extends('chat.layouts.master')


@section('inner-content')
<div class="block hero flex-column mb-0 bg-image" style="max-height: 300px;">
                    <!-- Chat #5 Header -->
                    <div class="block-header block-header-default w-100 bg-white-95 shadow-lg" style="min-height: 70px; height: 70px;">
                        <h3 class="block-title">
                            <img class="img-avatar img-avatar32" src="{{ asset('assets/media/avatars/avatar7.jpg') }}" alt="">
                            <span class="font-size-sm font-w600 ml-2">{{ ucfirst(App\Helper::getUserName($friendName)) }}</span>
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="fa fa-cog"></i>
                            </button>
                            <button type="button" class="btn-block-option d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
                                <i class="fa fa-users"></i>
                            </button>
                        </div>
                    </div>
                    <!-- END Chat #5 Header -->

                    <!-- Chat #5 Messages -->
                    <div class="js-chat-messages block-content block-content-full text-wrap-break-word overflow-y-auto w-100 flex-grow-1 px-lg-8 px-xlg-10 bg-white-90" data-chat-id="5" style="max-height: 350px;">
                      @foreach($message as $messages)
                      @if(Auth::user()->id != $messages->user_id)

                          <div class="mr-4">
                            <div class="d-inline-block font-w600 animated fadeIn bg-body-light border-3x px-3 py-2 mb-2 shadow-sm mw-100 border-left border-dark rounded-right">
                              {{ $messages->message }}
                            </div>
                         </div>
                         <div class=" font-italic text-muted animated fadeIn my-2" style="font-size: x-small;">{{ $messages->created_at->diffForHumans()}}</div>
                      @endif
                      @if(Auth::user()->id == $messages->user_id)

                          <div class="text-right ml-4">
                                <div class="d-inline-block font-w600 animated fadeIn bg-body-light border-3x px-3 py-2 mb-2 shadow-sm mw-100 border-right border-primary rounded-left text-left">
                                  {{ $messages->message }}
                            </div>
                          </div>
                          <div class=" font-italic text-muted animated fadeIn my-2 text-right" style="font-size: x-small;">{{ $messages->created_at->diffForHumans()}}</div>
                    @endif
                       @endforeach
                         <div id="appendMsg"></div>
                    </div>

                    <!-- Chat #5 Input -->
                    <div class="js-chat-form block-content p-3 w-100 d-flex align-items-center bg-white-95 shadow-lg" style="min-height: 70px; height: 70px;">
                        <form class="w-100" action="{{ route('chat.store')  }}" method="POST" id="storemessage">
                          <input type="hidden" value="{{ $friendName }}" name="friend_id">
                            <div class="input-group">

                                <div class="input-group-prepend d-sm-none dropup">
                                    <button type="button" class="btn btn-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="javascript:void(0)">
                                            <i class="fa fa-file-alt fa-fw mr-1"></i> Upload File
                                        </a>
                                        <a class="dropdown-item" href="javascript:void(0)">
                                            <i class="fa fa-image fa-fw mr-1"></i> Upload Image
                                        </a>
                                        <a class="dropdown-item" href="javascript:void(0)">
                                            <i class="fa fa-microphone-alt fa-fw mr-1"></i> Record Audio
                                        </a>
                                        <a class="dropdown-item" href="javascript:void(0)">
                                            <i class="fa fa-smile fa-fw mr-1"></i> Add Stickers
                                        </a>
                                    </div>
                                </div>
                                <div class="input-group-prepend d-none d-sm-flex">

                                </div>
                                <input type="text" name="message" class="js-chat-input form-control form-control-alt border-0 " data-target-chat-id="5" placeholder="Type a message..">
                                <!-- <div class="input-group-append">
                                    <button type="submit" class="btn btn-link">
                                        <i class="fab fa-telegram-plane"></i>
                                        <span class="d-none d-sm-inline ml-1 font-w600">Send</span>
                                    </button>
                                </div> -->
                            </div>
                        </form>
                    </div>
                    <!-- END Chat #5 Input -->
                </div>
@endsection
@section('javascript')
<script>


$(document).ready(function () {
$(".js-chat-messages").animate({ scrollTop: $('.js-chat-messages').prop("scrollHeight")}, 1000);
$('#storemessage').on( 'submit', function(e) {
        e.preventDefault();
        var message = $(this).find('input[name=message]').val();
        var friend_id = $(this).find('input[name=friend_id]').val();
        $.ajax({
            type: "POST",
            url: '/chat/store',
            data: { 'message' : message ,'_token' : "{{ csrf_token() }}" , 'friend_id' : friend_id},
            success: function (message) {
                 //alert(message);

              var msg='<div class="text-right ml-4">'+
                  '<div class="d-inline-block font-w600 animated fadeIn bg-body-light border-3x px-3 py-2 mb-2 shadow-sm mw-100 border-right border-primary rounded-left text-left">'+
                        message
                  '</div>'+
                  '</div>';
                  $('#appendMsg').append(msg);
                  $('input[name=message]').val('');
                  $(".js-chat-messages").animate({ scrollTop: $('.js-chat-messages').prop("scrollHeight")}, 1000);
                  }
        });
        });

  $('#storemessage').validate({ // initialize the plugin
      rules: {
        message : {
          required : true,
        },
      }
  });

});
</script>
@endsection

@extends('chat.layouts.master')
@section('inner-content')
<div class="bg-light">
                    <div class="hero">
                        <div class="hero-inner">
                            <div class="content content-full text-center">
                                <h1 class="display-4 font-w700 mb-3 js-appear-enabled animated fadeInDown" data-toggle="appear" data-class="animated fadeInDown">
                                    Chat<span class="text-success"> Application</span>
                                </h1>
                                <h2 class="font-w300 text-muted mb-5 js-appear-enabled animated fadeInUp" data-toggle="appear" data-class="animated fadeInUp" data-timeout="400">Chat with your Friend</h2>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

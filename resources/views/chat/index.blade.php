@include('chat.layouts.header')
@yield('custom_css')
   </head>
<body>
   <div class="page-container">
      @include('chat.layouts.sidebar')
         <div class="page-content">
          @include('chat.layouts.topbar')
            <div class="page-inner">
               <div id="main-wrapper">
                  <div class="pageheader pd-t-25 pd-b-35">
                     <div class="pd-t-5 pd-b-5">
                        <h1 class="pd-0 mg-0 tx-20">Sales Monitoring</h1>
                     </div>
                     <div class="breadcrumb pd-0 mg-0">
                        <a class="breadcrumb-item" href="index.html"><i class="icon ion-ios-home-outline"></i> Home</a>
                        <a class="breadcrumb-item" href="">Dashboard</a>
                     </div>
                  </div>
                  <div class="row row-xs clearfix">
                     <div class="col-sm-6 col-xl-3">
                        <div class="card mg-b-20">
                           <div class="card-body pd-y-0">
                              <div class="custom-fieldset mb-4">
                                 <div class="clearfix">
                                    <label>Today Orders</label>
                                 </div>
                                 <div class="d-flex align-items-center text-dark">
                                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded card-icon-warning">
                                       <i class="icon-screen-desktop tx-warning tx-20"></i>
                                    </div>
                                    <div>
                                       <h2 class="tx-20 tx-sm-18 tx-md-24 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark">$<span class="counter">5,300</span><small class="tx-15">.50</small></h2>
                                       <div class="d-flex align-items-center tx-gray-500"><span class="text-success mr-2 d-flex align-items-center"><i class="ion-android-arrow-up mr-1"></i>+451</span>avg. sales</div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-xl-3">
                        <div class="card mg-b-20">
                           <div class="card-body pd-y-0">
                              <div class="custom-fieldset mb-4">
                                 <div class="clearfix">
                                    <label>Today</label>
                                 </div>
                                 <div class="d-flex align-items-center text-dark">
                                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded card-icon-success">
                                       <i class="icon-diamond tx-success tx-20"></i>
                                    </div>
                                    <div>
                                       <h2 class="tx-20 tx-sm-18 tx-md-24 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark">$<span class="counter">1,500</span><small class="tx-15">.50</small></h2>
                                       <div class="d-flex align-items-center tx-gray-500"><span class="text-danger mr-2 d-flex align-items-center"><i class="ion-android-arrow-down mr-1"></i>-310</span>avg. sales</div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-xl-3">
                        <div class="card mg-b-20">
                           <div class="card-body pd-y-0">
                              <div class="custom-fieldset mb-4">
                                 <div class="clearfix">
                                    <label>Product Sold</label>
                                 </div>
                                 <div class="d-flex align-items-center text-dark">
                                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded card-icon-primary">
                                       <i class="icon-handbag tx-primary tx-20"></i>
                                    </div>
                                    <div>
                                       <h2 class="tx-20 tx-sm-18 tx-md-24 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark">$<span class="counter">4,900</span><small class="tx-15">.50</small></h2>
                                       <div class="d-flex align-items-center tx-gray-500"><span class="text-success mr-2 d-flex align-items-center"><i class="ion-android-arrow-up mr-1"></i>+350</span>avg. sales</div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-xl-3">
                        <div class="card mg-b-20">
                           <div class="card-body pd-y-0">
                              <div class="custom-fieldset mb-4">
                                 <div class="clearfix">
                                    <label>Total </label>
                                 </div>
                                 <div class="d-flex align-items-center text-dark">
                                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded card-icon-danger">
                                       <i class="icon-speedometer tx-danger tx-20"></i>
                                    </div>
                                    <div>
                                       <h2 class="tx-20 tx-sm-18 tx-md-24 mb-0 mt-2 mt-sm-0 tx-normal tx-rubik tx-dark">$<span class="counter">9,900</span><small class="tx-15">.50</small></h2>
                                       <div class="d-flex align-items-center tx-gray-500"><span class="text-danger mr-2 d-flex align-items-center"><i class="ion-android-arrow-down mr-1"></i>+130</span>avg. sales</div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
            </div>
@include('chat.layouts.footer')

@extends('chat.layouts.master')


@section('inner-content')


<div class="content">

<h1 class="content-heading">
Add Friend
</h1>


            <div class="block block-rounded block-bordered">
                 <div class="block-content block-content-full">
                    <div class="table-responsive">
                                <table class="table table-bordered table-striped table-vcenter datatable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Add Friend</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          @foreach($data as $datas)
                                              <tr>
                                                <td>{{  $datas->name }}</td>
                                                <td>
                                                  @php($checkFriend = App\AddFriend::checkFriend($datas->id))
                                                  @if(!empty($checkFriend))

                                                    @if($checkFriend->request_status == 1)
                                                    <button data-toggle="click-ripple" class="btn btn-hero-sm btn-hero-success">
                                                    Request Pending...
                                                    </button>
                                                    @elseif($checkFriend->request_status == 2)
                                                    <button data-toggle="click-ripple" class="btn btn-hero-sm btn-hero-success">
                                                    Request Accepted
                                                    </button>
                                                      @elseif($checkFriend->request_status == 3)
                                                        <button data-toggle="click-ripple" class="btn btn-hero-sm btn-hero-danger">
                                                      Request Rejected
                                                        </button>
                                                      @endif

                                                    @else
                                                    <form action="{{ route('storeRequest',$datas->id)  }}" method="post" >
                                                      @csrf
                                                      <button data-toggle="click-ripple" class="btn btn-hero-sm btn-hero-success">
                                                      <i class="fa fa-fw fa-plus mr-1"></i> Add Friend
                                                    </button>
                                                    </form>
                                                    @endif
                                                </td>
                                              </tr>
                                          @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                     </div>

</div>


@endsection

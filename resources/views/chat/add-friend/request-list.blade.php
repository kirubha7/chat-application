@extends('chat.layouts.master')


@section('inner-content')


<div class="content">

<h1 class="content-heading">
Friend Request List
</h1>


            <div class="block block-rounded block-bordered">
                 <div class="block-content block-content-full">
                    <div class="table-responsive">
                                <table class="table table-bordered table-striped table-vcenter datatable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          @foreach($data as $datas)
                                              <tr>
                                                <td>{{  ucfirst(App\Helper::getSenderName($datas->request_sender)) }}</td>
                                                <td>
                                                  <form action="{{ route('request.updateStatus',$datas->id)  }}" method="post">
                                                    @csrf
                                                  <button type="submit" data-toggle="click-ripple" name="status" value="2" class="btn btn-hero-sm btn-hero-success">
                                                   Accept
                                                 </button>
                                                  <button type="submit" data-toggle="click-ripple" name="status" value="3" class="btn btn-hero-sm btn-hero-danger">
                                                   Reject
                                                  </button>
                                                  </fom>
                                                </td>
                                              </tr>
                                          @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                     </div>

</div>


@endsection

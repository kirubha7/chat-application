<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::get('/dashboard',function(){
  return view('chat.dashboard');
})->name('dashboard');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Invite Friends
Route::resource('/invite-friends','InviteFriendsController');

//Add Friends
Route::resource('/add-friends','AddFriendsController');
Route::post('/store-request/{id}','AddFriendsController@storeRequest')->name('storeRequest');
Route::get('/request-list','AddFriendsController@requestList')->name('requestList');
Route::post('/request-list/status-update/{id}','AddFriendsController@updateStatus')->name('request.updateStatus');

//chat
Route::resource('/chat','ChatController');
Route::post('/chat/store','ChatController@store');
Route::get('/chat/chat-with-friend/{id}','ChatController@chatWithFriend')->name('chat.chatWithFriend');
